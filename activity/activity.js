// PART 1

db.users.find({
	$or: [
	{"firstName": {$regex: "s", $options: "$i"}},
	{"lastName": {$regex: "d", $options: "$i"}}
]},
	{
        "_id": 0,   
		"firstName": 1,
        "lastName": 1
	}
);

// PART 2

db.users.find({
	$and: [
		{"department": "HR"},
		{"age": {
			$gte: 70
		}}
	]
});

// PART 3

db.users.find({
	$or: [
			{"firstName": {$regex: "e", $options: "$i"}},
			{"age": {
				$lte: 30
			}}
		 ]
})
